function cells (number){
switch (number){
    case 0:
        x=app.screen.width/8;
        y=app.screen.height/8-50;
        return {x:x,y:y}
    case 1:
        x=app.screen.width/8+app.screen.width/4;
        y=app.screen.height/8-50;
        return {x:x,y:y}
    case 2:
        x=app.screen.width/8+app.screen.width/2;
        y=app.screen.height/8-50;
        return {x:x,y:y};
    case 3:
        x=app.screen.width - app.screen.width/8;
        y=app.screen.height/8-50;
        return {x:x,y:y};
    case 4:
        x=app.screen.width/8;
        y=app.screen.height/8+app.screen.height/4-100;
        return {x:x,y:y};
    case 5:
        x=app.screen.width/8+app.screen.width/4;
        y=app.screen.height/8+app.screen.height/4-100;
        return {x:x,y:y};
    case 6:
        x=app.screen.width/8+app.screen.width/2;
        y=app.screen.height/8+app.screen.height/4-100;
        return {x:x,y:y};
    case 7:
        x=app.screen.width - app.screen.width/8;
        y=app.screen.height/8+app.screen.height/4-100;
        return {x:x,y:y};
    case 8:
        x=app.screen.width/8;
        y=app.screen.height/8+app.screen.height/2-150;
        return {x:x,y:y};
    case 9:
        x=app.screen.width/8+app.screen.width/4;
        y=app.screen.height/8+app.screen.height/2-150;
        return {x:x,y:y};
    case 10:
        x=app.screen.width/8+app.screen.width/2;
        y=app.screen.height/8+app.screen.height/2-150;
        return {x:x,y:y};
    case 11:
        x=app.screen.width - app.screen.width/8;
        y=app.screen.height/8+app.screen.height/2-150;
        return {x:x,y:y};
    case 12:
        x=app.screen.width/8;
        y=app.screen.height-app.screen.height/8-200;
        return {x:x,y:y};
    case 13:
        x=app.screen.width/8+app.screen.width/4;
        y=app.screen.height-app.screen.height/8-200;
        return {x:x,y:y};
    case 14:
        x=app.screen.width/8+app.screen.width/2;
        y=app.screen.height-app.screen.height/8-200;
        return {x:x,y:y};
    case 15:
        x=app.screen.width - app.screen.width/8;
        y=app.screen.height-app.screen.height/8-200;
        return {x:x,y:y};
    }
}

function createForm(x,y,formNum){   
	let shape;
	switch (formNum){
		case 1:
			shape = new Rectangle(x,y);
			shape.random_draw();
			app.stage.addChild(shape);
			break;
		case 2: 
			shape = new Circle(x,y);
			shape.random_draw();
			app.stage.addChild(shape);
			break;
		case 3:
			shape = new Triangle(x,y);
			shape.random_draw();
			app.stage.addChild(shape);
			break;
	}
}

function onDragStart(event) {
    this.data = event.data;
    this.alpha = 0.5;
    if (this instanceof Rectangle){
        this.pivot.x = 75;
        this.pivot.y = 75;
    }
    else if (this instanceof Triangle){
        this.pivot.x = 100;
        this.pivot.y = 75;
    }
    this.dragging = true;
}

function onDragEnd() {
	if (this instanceof Rectangle){
    	if (Collision(this,rectangle)){
    		app.stage.removeChild(this); }
    		else {
    			this.x=this.Oldx;
    			this.y=this.Oldy;}}
   	else if (this instanceof Circle){
     	if (Collision(this,circle)){
    		app.stage.removeChild(this);}  
    		else {
    			this.x=this.Oldx;
    			this.y=this.Oldy;}}
    else if (this instanceof Triangle){
    	if (Collision(this,triangle)){
    		app.stage.removeChild(this);}
    		else {
    			this.x=this.Oldx;
    			this.y=this.Oldy;}}
    this.alpha = 1;
    this.dragging = false;
    this.data = null;
    }

function onDragMove() {
    if (this.dragging) {
        const newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x;
        this.y = newPosition.y;
    }
}
function Collision(first_obj, second_obj){
	let first_body = first_obj.getBounds();
	let second_body = second_obj.getBounds();

	return first_body.x + first_body.width > second_body.x &&
		   first_body.x < second_body.x + second_body.width &&
		   first_body.y + first_body.height > second_body.y &&
		   first_body.y < second_body.y + second_body.height;
}
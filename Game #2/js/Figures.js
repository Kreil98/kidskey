class Figures extends PIXI.Graphics {
  constructor(x, y) {
    super();
    this.x = x;
    this.y = y;
  }
}

class Rectangle extends Figures {
  constructor(x, y) {
    super(x, y);
  }
  draw(fieldWight, fieldHeight) {
    this.beginFill(0xffffff);
    this.lineStyle(3, 0x000000, 1);
    this.drawRect(0, 0, fieldWight, fieldHeight);
    this.endFill();
  }
}

class RowLine extends Figures {
  constructor(x, y) {
    super(x, y);
    this.Oldx = x;
    this.Oldy = y;
  }
  draw(rowSize) {
    this.lineStyle(1, 0x0000ff, 1);
    this.moveTo(0, 0);
    this.lineTo(rowSize, 0);
  }
}

class ColLine extends Figures {
  constructor(x, y) {
    super(x, y);
    this.Oldx = x;
    this.Oldy = y;
  }
  draw(colSize) {
    this.lineStyle(1, 0x0000ff, 1);
    this.moveTo(0, 0);
    this.lineTo(0, colSize);
  }
}

class Dot extends Figures {
  constructor(x, y) {
    super(x, y);
    this.Oldx = x;
    this.Oldy = y;
  }
  draw() {
    this.beginFill(0x000000);
    this.drawCircle(0, 0, 5, 1);
    this.endFill();
  }
  action() {
    this.interactive = true;
    this.buttonMode = true;
    this.on("pointerdown", onDragStart)
      .on("pointerup", onDragEnd)
      .on("pointerupoutside", onDragEnd)
      .on("pointermove", onDragMove);
  }
}

class VerticalLine extends Figures {
  constructor(x, y) {
    super(x, y);
    this.Oldx = x;
    this.Oldy = y;
  }
  draw() {
    this.beginFill(0x000000);
    this.lineStyle(1, 0x000000, 1);
    this.moveTo(0, 0);
    this.lineTo(0, cellSize);
    this.lineTo(3, cellSize);
    this.lineTo(3, 0);
    this.closePath();
    this.endFill();
  }
  action() {
    this.pivot.x = 0;
    this.pivot.y = 0;
    this.interactive = true;
    this.buttonMode = true;
    this.on("pointerdown", onDragStart)
      .on("pointerup", onDragEnd)
      .on("pointerupoutside", onDragEnd)
      .on("pointermove", onDragMove);
  }
}

class HorizontalLine extends Figures {
  constructor(x, y) {
    super(x, y);
    this.Oldx = x;
    this.Oldy = y;
  }
  draw() {
    this.beginFill(0x000000);
    this.lineStyle(1, 0x000000, 1);
    this.moveTo(0, 0);
    this.lineTo(cellSize, 0);
    this.lineTo(cellSize, 3);
    this.lineTo(0, 3);
    this.closePath();
    this.endFill();
  }
  action() {
    this.pivot.x = 0;
    this.pivot.y = 0;
    this.interactive = true;
		this.buttonMode = true;
		this
			  .on('pointerdown', onDragStart)
        	.on('pointerup', onDragEnd)
        	.on('pointerupoutside', onDragEnd)
        	.on('pointermove', onDragMove);
  }
}

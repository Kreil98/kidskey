class FormFigure extends PIXI.Graphics {
	constructor(x,y){
		super();
		this.x=x;
		this.y=y;
		this.Oldx=x;
		this.Oldy=y;
	}
}

class Rectangle extends FormFigure {
	constructor(x,y){
		super(x,y);
	}
	draw(){
	this.beginFill(0xFFFFFF);
	this.lineStyle(2,0x000000,1);
	this.drawRect(0,0,150,150);
	this.endFill();
}
	random_draw(){
		this.scale.set(Math.random() * 0.3+0.5);
		this.rotation = Math.random() * 10;
		this.beginFill(Math.random() * 0xFFFFFF);
		this.lineStyle(2,Math.random() * 0xFFFFFF,1);
		this.drawRect(0,0,150,150);
		this.endFill();
		this.interactive = true;
		this.buttonMode = true;
		this
			.on('pointerdown', onDragStart)
        	.on('pointerup', onDragEnd)
        	.on('pointerupoutside', onDragEnd)
        	.on('pointermove', onDragMove);
			}}

class Circle extends FormFigure {
	constructor(x,y){
		super(x,y);
	}
	draw(){
	this.beginFill(0xFFFFFF);
	this.drawCircle(0,0,75,10);
	this.endFill();
}
	random_draw(){
		this.scale.set(Math.random() * 0.3+0.5);
		this.rotation = Math.random() * 10;
		this.lineStyle(2,Math.random() * 0xFFFFFF,1);
		this.beginFill(Math.random() * 0xFFFFFF);
		this.drawCircle(0,0,75,10);
		this.endFill();
		this.interactive = true;
		this.buttonMode = true;
		this
			.on('pointerdown', onDragStart)
        	.on('pointerup', onDragEnd)
        	.on('pointerupoutside', onDragEnd)
        	.on('pointermove', onDragMove);
			}}
	
class Triangle extends FormFigure {
	constructor(x,y){
		super(x,y);
	}
	draw(){
	this.beginFill(0xFFFFFF);
	this.lineStyle(2, 0x000000, 1);
	this.moveTo(100, 0);
	this.lineTo(150, 100);
	this.lineTo(50, 100);
	this.lineTo(100, 0);
	this.closePath();
	this.endFill();
}
	random_draw(){
		this.scale.set(Math.random() * 0.3+0.5);
		this.rotation = Math.random() * 10;
		this.lineStyle(2,Math.random() * 0xFFFFFF,1);
		this.beginFill(Math.random() * 0xFFFFFF);
		this.moveTo(100, 0);
		this.lineTo(150, 100);
		this.lineTo(50, 100);
		this.lineTo(100, 0);
		this.closePath();
		this.endFill();
		this.interactive = true;
		this.buttonMode = true;
		this
			.on('pointerdown', onDragStart)
        	.on('pointerup', onDragEnd)
        	.on('pointerupoutside', onDragEnd)
        	.on('pointermove', onDragMove);
			}}

class Line extends FormFigure {
	constructor(x,y){
		super(x,y);
	}
	draw(){
	this.lineStyle(4,0x000000,1);
	this.moveTo(0,0);
	this.lineTo(app.screen.width,0);
}}
